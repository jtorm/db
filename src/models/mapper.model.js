/*! (c) jTorm and other contributors | https://jtorm.com/license */

const
  mapperModel = function(dataObject, schemaObject, isReverse) {
    const resultObj = {};

    if (isReverse) {
      for (let k in schemaObject) {
        if (dataObject[k] !== undefined) {
          resultObj[k] = dataObject[k];
        } else if (dataObject[schemaObject[k]] !== undefined) {
          resultObj[k] = dataObject[schemaObject[k]];
        }
      }
    } else {
      for (let k in schemaObject) {
        if (dataObject[k] !== undefined) {
          resultObj[schemaObject[k]] = dataObject[k];
        } else if (dataObject[schemaObject[k]] !== undefined) {
          resultObj[schemaObject[k]] = dataObject[schemaObject[k]];
        }
      }
    }

    return resultObj;
  }
;

module.exports = {
  mapperModel
};
