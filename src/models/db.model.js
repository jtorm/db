/*! (c) jTorm and other contributors | https://jtorm.com/license */

const dbModel = {
  instance: null,

  create: function (dbName, tableName, recordObj) {
    return dbModel.instance.create(dbName, tableName, recordObj);
  },

  read: function (dbName, tableName, filterObj, selectArray) {
    return dbModel.instance.read(dbName, tableName, filterObj, selectArray);
  },

  update: function (dbName, tableName, selectObj, updateObj) {
    return dbModel.instance.update(dbName, tableName, selectObj, updateObj);
  },

  delete: function (dbName, tableName, selectObj) {
    return dbModel.instance.delete(dbName, tableName, selectObj);
  },

  createDb: function (dbName) {
    return dbModel.instance.createDb(dbName);
  },

  createTable: function (dbName, tableName) {
    return dbModel.instance.createTable(dbName, tableName);
  },

  dropTable: function (dbName, tableName) {
    return dbModel.instance.dropTable(dbName, tableName);
  }
};

module.exports = {
  dbModel
};
