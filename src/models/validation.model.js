/*! (c) jTorm and other contributors | https://jtorm.com/license */

const
  Joi = require('joi'),

  validationModel = {
    validate: function (data, schema) {
      schema = Joi.object(schema);

      const { value, error } = schema.validate(data, {
        convert: true
      });

      if (error) {
        throw error;
      }

      return value;
    }
  }
;

module.exports = {
  validationModel
};
