/*! (c) jTorm and other contributors | https://jtorm.com/license */

const
  { EntityDoesNotExistsError } = require('@jtorm/error-handler'),
  { dbModel } = require('./../../../db.model'),
  { mapperModel } = require('./../../../mapper.model'),
  { validationModel } = require('./../../../validation.model'),
  dbReadCollectionActionModel = (db, model, queryObject) => {
    return new Promise(async (res, rej) => {
      try {
        let convertedRecord = validationModel.validate(queryObject, model.validationSchema);
        convertedRecord = mapperModel(convertedRecord, model.schema);

        const result = await dbModel.read(db, model.tableName, convertedRecord, model.collectionSchema);

        if (!result.length)
          return rej(new EntityDoesNotExistsError)
        ;

        for (let k in result)
          result[k] = mapperModel(result[k], model.schema, 1)
        ;

        return res(result);
      } catch (e) {
        return rej(e);
      }
    });
  }
;

module.exports = {
  dbReadCollectionActionModel
};
