/*! (c) jTorm and other contributors | https://jtorm.com/license */

const
  { EntityAlreadyExistsError } = require('@jtorm/error-handler'),
  { dbModel } = require('./../../../db.model'),
  { dbUniqueResolverModel } = require('./../../../db/db-unique-resolver.model'),
  { mapperModel } = require('./../../../mapper.model'),
  { validationModel } = require('./../../../validation.model'),
  dbCreateUniqueActionModel = (db, model, recordObject) => {
    return new Promise(async (res, rej) => {
      try {
        let convertedRecord = validationModel.validate(recordObject, model.validationSchema);
        convertedRecord = mapperModel(convertedRecord, model.schema);

        const
          date = new Date(),
          isUnique = await dbUniqueResolverModel(db, model, convertedRecord)
        ;

        if (!isUnique)
          return rej(new EntityAlreadyExistsError)
        ;

        if (model.schema.dateCreated && !convertedRecord[model.schema.dateCreated])
          convertedRecord[model.schema.dateCreated] = date.toISOString()
        ;

        if (model.schema.dateUpdated && !convertedRecord[model.schema.dateUpdated])
          convertedRecord[model.schema.dateUpdated] = date.toISOString()
        ;

        const result = await dbModel.create(db, model.tableName, convertedRecord);

        return res(result);
      } catch (e) {
        return rej(e);
      }
    });
  }
;

module.exports = {
  dbCreateUniqueActionModel
};
