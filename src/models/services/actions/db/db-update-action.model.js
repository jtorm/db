/*! (c) jTorm and other contributors | https://jtorm.com/license */

const
  {ValidationError} = require('joi'),
  { dbModel } = require('./../../../db.model'),
  { mapperModel } = require('./../../../mapper.model'),
  { validationModel } = require('./../../../validation.model'),
  dbUpdateActionModel = (db, model, recordObject) => {
    return new Promise(async (res, rej) => {
      try {
        if (!recordObject.id)
          throw new ValidationError('ID not set')
        ;

        let convertedRecord = validationModel.validate(recordObject, model.validationSchema);
        convertedRecord = mapperModel(convertedRecord, model.schema);

        if (model.schema.dateUpdated)
          convertedRecord[model.schema.dateUpdated] = (new Date()).toISOString()
        ;

        const result = await dbModel.update(db, model.tableName, {id: recordObject.id}, convertedRecord);

        return res(result);
      } catch (e) {
        return rej(e);
      }
    });
  }
;

module.exports = {
  dbUpdateActionModel
};
