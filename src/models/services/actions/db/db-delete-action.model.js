/*! (c) jTorm and other contributors | https://jtorm.com/license */

const
  { dbModel } = require('./../../../db.model'),
  { mapperModel } = require('./../../../mapper.model'),
  { validationModel } = require('./../../../validation.model'),
  dbDeleteActionModel = (db, model, recordObject) => {
    return new Promise(async (res, rej) => {
      try {
        let convertedRecord = validationModel.validate(recordObject, model.validationSchema);
        convertedRecord = mapperModel(convertedRecord, model.schema);

        const result = await dbModel.delete(db, model.tableName, convertedRecord);

        return res(result);
      } catch (e) {
        return rej(e);
      }
    });
  }
;

module.exports = {
  dbDeleteActionModel
};
