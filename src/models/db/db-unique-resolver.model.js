/*! (c) jTorm and other contributors | https://jtorm.com/license */

const
  { dbReadActionModel } = require('./../services/actions/db/db-read-action.model'),
  dbUniqueResolverModel = async (db, model, recordObject) => {
    for (let k in model.unique) {
      try {
        const queryObject = {};

        if (typeof model.unique[k] === 'object') {
          for (let k2 in model.unique[k]) {
            if (recordObject[model.unique[k][k2]] === undefined)
              continue;

            queryObject[model.unique[k][k2]] = recordObject[model.unique[k][k2]];
          }
        } else {
          if (recordObject[model.unique[k]] === undefined)
            continue;

          queryObject[model.unique[k]] = recordObject[model.unique[k]];
        }

        await dbReadActionModel(db, model, queryObject);

        return false;
      } catch (e) {

      }
    }

    return true;
  }
;

module.exports = {
  dbUniqueResolverModel
};
