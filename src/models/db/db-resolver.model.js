/*! (c) jTorm and other contributors | https://jtorm.com/license */

const
  dataDbResolverModel = function(i18n) {
    return process.env.DB_SCHEMA_NAME + '_' + i18n;
  }
;

module.exports = {
  dataDbResolverModel
};
