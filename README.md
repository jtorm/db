# jTorm DB

DB models for interacting with databases and managing schema.org schemas.

## Install

```js
npm install @jtorm/db
```

## Config

.env config variable `DB_SCHEMA_NAME` that defines the db name of the schema.org schemas, the locale will be suffixed to it.
